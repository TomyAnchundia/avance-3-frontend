import { React, useState, useEffect } from "react";
import "./css/registrarse.css";
import {useNavigate} from 'react-router-dom'

const Registrarse = () => {
  //useState
  const [docente, setDocente] = useState({
    nombre_docente: "",
    apellido_docente: "",
    correo_docente: "",
    telefono_docente: "",
    nombre_usuario: "",
    contrasena_usuario: "",
    repetir_contrasena: "",
    usuario_admin: "false"
  });

//estado de cargando
  const [loading, setLoading] = useState (false)

  //definiendo el usenavigate
  const navigate = useNavigate ();


  //evento de captura de datos
  const handleSubmit = async (e) => {
    e.preventDefault();

    //establecemos el loading
    setLoading(true);
    const res = await fetch("http://localhost:4000/docente", {
      method: "POST",
      body: JSON.stringify(docente),
      headers: {"Content-Type": "application/json"},
    });
    const data = await res.json();
    console.log(data);

    setLoading(false);
    navigate('/');
  };

  const handleChange = (e) => {
    setDocente({ ...docente, [e.target.name]: e.target.value });
  };
  return (
    <div className="container-registrarse">
      <h1> Registrate:</h1>
      <form className="registrarse" onSubmit={handleSubmit}>
        <div className="date">
          <input
            type="text"
            required
            onChange={handleChange}
            name="nombre_docente"
          />
          <span></span>
          <label>Nombres</label>
        </div>
        <div className="date">
          <input
            type="text"
            required
            onChange={handleChange}
            name="apellido_docente"
          />
          <span></span>
          <label>Apellidos</label>
        </div>
        <div className="date">
          <input
            type="text"
            required
            onChange={handleChange}
            name="correo_docente"
          />
          <span></span>
          <label>Correo electronico</label>
        </div>
        <div className="date">
          <input
            type="text"
            required
            onChange={handleChange}
            name="telefono_docente"
          />
          <span></span>
          <label>Telefono</label>
        </div>
        <div className="date">
          <input
            type="text"
            required
            onChange={handleChange}
            name="nombre_usuario"
          />
          <span></span>
          <label>Nombre de Usuario</label>
        </div>
        <div className="date">
          <input
            type="password"
            required
            onChange={handleChange}
            name="contrasena_usuario"
          />
          <span></span>
          <label>Contrasena</label>
        </div>
        <div className="date">
          <input
            type="password"
            required
            onChange={handleChange}
            name="repetir_contrasena"
          />
          <span></span>
          <label>Repetir Contrasena</label>
        </div>
        <input className="btn-login" type="submit" value="Registrar" />
        <div className="link-registrarse">
          Ya estas registrado?{" "}
          <a className="enlace-iniciar sesion" href="/">
            {" "}
            Inicar sesion
          </a>
        </div>
      </form>
    </div>
  );
};

export default Registrarse;
