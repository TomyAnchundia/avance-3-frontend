import React from 'react';
import './css/styles.css';
import logo from '../images/FacciLOGO.png';


const Navbar = () => {
    return (
        <header className="nav">
            <nav className="navbar">

                <a href={'/'}> <img className="logo-nav" src={logo} /> </a>
                <a className="nav-gest-lab" href={'/'} > <h5>GESTION DE LABORATORIOS</h5>  </a>
                <ul className="nav-menu">

                </ul>

            </nav>


        </header>


    )
}

export default Navbar
